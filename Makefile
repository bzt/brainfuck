TARGET = bf

all: $(TARGET)

$(TARGET): bf.c
	gcc -ansi -pedantic -Wall -Wextra -O3 bf.c -o $(TARGET)

clean:
	rm $(TARGET) *.o 2>/dev/null || true
